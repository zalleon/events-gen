package main

import (
	"context"
	"database/sql"
	"fmt"
	"math"
	"math/rand"
	"os"
	"sync"
	"time"

	"github.com/google/logger"
	"github.com/google/uuid"
	"github.com/mailru/go-clickhouse"
	_ "github.com/mailru/go-clickhouse"
	"github.com/pkg/errors"
)

/*

 */

const (
	AccountsCount            = 100
	MaxAccountUsersCount     = 100000
	BroadcastMessagesCount   = 25
	TransactionMessagesCount = 10000

	BatchSize = 2000
)

type Event struct {
	Date            string
	Time            int64
	EventName       string
	EventAttributes string
	AccountID       uint64
	UserID          string
	MessageID       string
}

type Account struct {
	Uuid       uint64
	Users      []User
	UsersCount int
}

type User struct {
	Uuid string
}

// kubectl describe svc clickhouse
// ssh -f -N root@scow-nuc03.nuc-lan -L 8123:10.233.64.42:8123

func main() {
	l := logger.Init("events-gen", true, false, os.Stdout)

	clickHouseWriteConnection, err := sql.Open("clickhouse", "http://localhost:8123?debug=false")
	if err != nil {
		l.Fatal(errors.Wrap(err, "failed to open click house write connection"))
	}

	var wg sync.WaitGroup

	wg.Add(1)
	eventChan := make(chan Event, 1000)
	limiter := time.Tick(2 * time.Second)

	go writer(eventChan, limiter, &wg, clickHouseWriteConnection, l)

	accounts := fillAccounts()
	events := []string{"MessageSent", "MessageDelivered", "MessageOpened"}

	// 3 month dataset
	startDate := time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC)
	endDate := time.Date(2019, 4, 1, 0, 0, 0, 0, time.UTC)

	// Iterate by days
	// Each day n accounts make n broadcast pushes and n transaction pushes
	for d := startDate; d.Before(endDate); d = d.AddDate(0, 0, 1) {
		// Rand count accounts
		accountIteration := rand.Intn(AccountsCount)
		for acc := 0; acc < accountIteration; acc++ {
			account := accounts[randIndex(len(accounts))]
			broadcastIt := rand.Intn(BroadcastMessagesCount)
			for broadcastMessagesCount := 0; broadcastMessagesCount < broadcastIt; broadcastMessagesCount++ {
				broadcastMessagesSize := rand.Intn(account.UsersCount)

				conversionDelivery := rand.Intn(10) + 85
				conversionOpen := rand.Intn(5) + 1
				messageID := uuid.New().String()

				startTime := int64(rand.Intn(60 * 60 * 12))

				for u := 0; u < broadcastMessagesSize; u++ {
					userID := account.Users[randIndex(account.UsersCount)].Uuid
					sentTime := d.Unix() + startTime + int64(math.Log(float64(u)))

					e := Event{
						Date:            d.Format("2006-01-02"),
						Time:            sentTime,
						EventName:       events[0], // Message sent
						EventAttributes: "{}",
						AccountID:       account.Uuid,
						UserID:          userID,
						MessageID:       messageID,
					}
					eventChan <- e

					if ver(broadcastMessagesSize, conversionDelivery) {
						e := Event{
							Date:            d.Format("2006-01-02"),
							Time:            sentTime + rand.Int63n(3),
							EventName:       events[1], // Message delivered
							EventAttributes: "{}",
							AccountID:       account.Uuid,
							UserID:          userID,
							MessageID:       messageID,
						}
						eventChan <- e
					}

					if ver(broadcastMessagesSize, conversionOpen) {
						e := Event{
							Date:            d.Format("2006-01-02"),
							Time:            sentTime + rand.Int63n(60*60*3),
							EventName:       events[2], // Message opened
							EventAttributes: "{}",
							AccountID:       account.Uuid,
							UserID:          userID,
							MessageID:       messageID,
						}
						eventChan <- e
					}
				}
			}
		}
	}

	close(eventChan)

	wg.Wait()
}

func writer(in chan Event, rateLimiter <-chan time.Time, wg *sync.WaitGroup, cnn *sql.DB, l *logger.Logger) {
	defer wg.Done()

	var buffer []Event

	for e := range in {
		buffer = append(buffer, e)

		if len(buffer) >= BatchSize {
			//<- rateLimiter
			err := insert(context.Background(), cnn, buffer)
			if err != nil {
				l.Error(err)
			}

			// Clear buffer
			buffer = []Event{}
			l.Info("Flush buffer")
		}
	}
}

func insert(ctx context.Context, cnn *sql.DB, items []Event) error {
	ctx = context.WithValue(ctx, clickhouse.QueryID, fmt.Sprintf("%s-%d", "event-gen", rand.Uint64()))
	ctx, cancelFunc := context.WithTimeout(ctx, 10*time.Second)

	defer func() {
		cancelFunc()
	}()

	tx, err := cnn.BeginTx(ctx, nil)
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare(fmt.Sprintf("INSERT INTO %s.%s (date,time,event_name,event_attributes,account_id,user_id, message_id) VALUES (?,?,?,?,?,?,?)", "default", "user_event_history"))
	if err != nil {
		_ = tx.Rollback()
		return err
	}

	for i := range items {
		if _, err = stmt.Exec(
			items[i].Date,
			items[i].Time,
			items[i].EventName,
			items[i].EventAttributes,
			items[i].AccountID,
			items[i].UserID,
			items[i].MessageID,
		); err != nil {
			_ = tx.Rollback()
			return err
		}
	}

	if err = tx.Commit(); err != nil {
		_ = tx.Rollback()
		return err
	}

	return stmt.Close()
}

func fillAccounts() []Account {
	var accounts []Account
	for count := 0; count < AccountsCount; count++ {
		users := fillUsers()
		accounts = append(accounts, Account{Uuid: rand.Uint64(), Users: users, UsersCount: len(users)})
	}
	return accounts
}

func fillUsers() []User {
	var users []User
	for count := 0; count < rand.Intn(MaxAccountUsersCount)+1; count++ {
		users = append(users, User{Uuid: "user-" + uuid.New().String()})
	}
	return users
}

func randIndex(len int) int {
	switch len {
	case 0:
		return 0
	case 1:
		return 0
	default:
		return rand.Intn(len - 1)
	}
}

func ver(base, percent int) bool {
	return rand.Intn(base) < (base/100)*percent
}
