CREATE TABLE default.user_event_history
(
    `date` Date DEFAULT CAST(now(), 'Date'),
    `time` DateTime DEFAULT now(),
    `event_name` String,
    `event_attributes` String,
    `account_id` Int64,
    `message_id` String,
    `user_id` String,
    INDEX user_idx user_id TYPE set(0) GRANULARITY 3,
    INDEX message_idx message_id TYPE set(0) GRANULARITY 3
)
ENGINE = MergeTree()
PARTITION BY toYYYYMMDD(date)
ORDER BY (account_id, event_name, message_id, user_id)
SETTINGS index_granularity = 8192