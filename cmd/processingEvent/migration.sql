CREATE TABLE default.processing
(
    `dateTime`              DateTime,
    `processingStartAt`     DateTime,
    `accountId`             UInt64,
    `messageHash`           String,
    `messageHeader`         Nullable(String) CODEC (ZSTD(1)),
    `messageId`             Nullable(UInt64),
    `applicationCode`       Nullable(String),
    `applicationGroupCode`  Nullable(String),
    `environment`           Nullable(String),
    `flowId`                String,
    `flowType`              String,
    `initialDeviceCount`    UInt64,
    `initialUserCount`      UInt64,
    `filteredCount`         UInt64,
    `processedCount`        UInt64,
    `sentCount`             UInt64,
    `confirmSentCount`      UInt64,
    `confirmDeliveredCount` UInt64,
    `complainedCount`       UInt64,
    `bouncedCount`          UInt64,
    INDEX                   processingStartAt_minmax processingStartAt TYPE minmax GRANULARITY 3
) ENGINE = SummingMergeTree((filteredCount, processedCount, sentCount, confirmSentCount, confirmDeliveredCount, complainedCount, bouncedCount))
    PARTITION BY toYYYYMMDD(dateTime)
    ORDER BY (accountId, messageHash)
    SETTINGS index_granularity = 8192