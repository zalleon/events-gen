package main

import (
	"context"
	"database/sql"
	"fmt"
	"math"
	"math/rand"
	"os"
	"sync"
	"time"

	"github.com/google/logger"
	"github.com/google/uuid"
	"github.com/mailru/go-clickhouse"
	_ "github.com/mailru/go-clickhouse"
	"github.com/pkg/errors"
)

const (
	AccountsCount          = 500
	MaxAccountUsersCount   = 100
	BroadcastMessagesCount = 25000

	BatchSize = 2000
)

type Event struct {
	DateTime              int64
	ProcessingStartAt     int64
	AccountID             uint64
	MessageHash           string
	MessageHeader         string
	MessageId             uint64
	applicationCode       string
	applicationGroupCode  string
	environment           string
	flowId                string
	flowType              string
	initialDeviceCount    uint64
	initialUserCount      uint64
	filteredCount         uint64
	processedCount        uint64
	sentCount             uint64
	confirmSentCount      uint64
	confirmDeliveredCount uint64
	complainedCount       uint64
	bouncedCount          uint64
}

type Account struct {
	Uuid       uint64
	Users      []User
	UsersCount int
}

type User struct {
	Uuid string
}

// kubectl describe svc clickhouse
// ssh -f -N root@scow-nuc03.nuc-lan -L 8123:10.233.64.42:8123

func main() {
	l := logger.Init("events-gen", true, false, os.Stdout)

	clickHouseWriteConnection, err := sql.Open("clickhouse", "http://localhost:8123?debug=false")
	if err != nil {
		l.Fatal(errors.Wrap(err, "failed to open click house write connection"))
	}

	var wg sync.WaitGroup

	wg.Add(1)
	eventChan := make(chan Event, 1000)
	limiter := time.Tick(2 * time.Second)

	go writer(eventChan, limiter, &wg, clickHouseWriteConnection, l)

	accounts := fillAccounts()

	// 3 month dataset
	startDate := time.Date(2019, 1, 1, 0, 0, 0, 0, time.UTC)
	endDate := time.Date(2019, 1, 7, 0, 0, 0, 0, time.UTC)

	// Iterate by days
	// Each day n accounts make n broadcast pushes and n transaction pushes
	for d := startDate; d.Before(endDate); d = d.AddDate(0, 0, 1) {
		// Rand count accounts
		accountIteration := rand.Intn(AccountsCount)
		for acc := 0; acc < accountIteration; acc++ {
			account := accounts[randIndex(len(accounts))]
			broadcastIt := rand.Intn(BroadcastMessagesCount)
			for broadcastMessagesCount := 0; broadcastMessagesCount < broadcastIt; broadcastMessagesCount++ {
				broadcastMessagesSize := rand.Intn(account.UsersCount)

				conversionConfirmSent := rand.Intn(10) + 90
				conversionConfirmDelivered := rand.Intn(10) + 85
				conversionComplained := rand.Intn(5) + 1
				conversionBounced := rand.Intn(2) + 1

				messageHash := uuid.New().String()

				startTime := int64(rand.Intn(60 * 60 * 12))

				e := Event{
					DateTime:          d.Unix() + startTime,
					ProcessingStartAt: d.Unix() + startTime,
					AccountID:         account.Uuid,
					MessageHash:       messageHash,
					filteredCount:     uint64(broadcastMessagesSize),
					processedCount:    uint64(broadcastMessagesSize),
				}
				eventChan <- e

				// Failed broadcast messages in 1% cases
				if ver(broadcastIt, 1) {
					continue
				}

				for u := 0; u < broadcastMessagesSize; u++ {
					sentTime := d.Unix() + startTime + int64(math.Log(float64(u)))

					e := Event{
						DateTime:    sentTime,
						AccountID:   account.Uuid,
						MessageHash: messageHash,
						sentCount:   1,
					}
					eventChan <- e

					if ver(broadcastMessagesSize, conversionConfirmSent) {
						confirmSentTime := sentTime + rand.Int63n(3)
						e := Event{
							DateTime:         confirmSentTime,
							AccountID:        account.Uuid,
							MessageHash:      messageHash,
							confirmSentCount: 1,
						}
						eventChan <- e

						if ver(broadcastMessagesSize, conversionConfirmDelivered) {
							e := Event{
								DateTime:              confirmSentTime + rand.Int63n(60*60*3),
								AccountID:             account.Uuid,
								MessageHash:           messageHash,
								confirmDeliveredCount: 1,
							}
							eventChan <- e
						}

						if ver(broadcastMessagesSize, conversionComplained) {
							e := Event{
								DateTime:        confirmSentTime + rand.Int63n(60*60*3),
								AccountID:       account.Uuid,
								MessageHash:     messageHash,
								complainedCount: 1,
							}
							eventChan <- e
						}

						if ver(broadcastMessagesSize, conversionBounced) {
							e := Event{
								DateTime:     confirmSentTime + rand.Int63n(60*60*3),
								AccountID:    account.Uuid,
								MessageHash:  messageHash,
								bouncedCount: 1,
							}
							eventChan <- e
						}
					}
				}
			}
		}
	}

	close(eventChan)

	wg.Wait()
}

func writer(in chan Event, rateLimiter <-chan time.Time, wg *sync.WaitGroup, cnn *sql.DB, l *logger.Logger) {
	defer wg.Done()

	var buffer []Event

	for e := range in {
		buffer = append(buffer, e)

		if len(buffer) >= BatchSize {
			//<- rateLimiter
			err := insert(context.Background(), cnn, buffer)
			if err != nil {
				l.Error(err)
			}

			// Clear buffer
			buffer = []Event{}
			l.Info("Flush buffer")
		}
	}
}

func insert(ctx context.Context, cnn *sql.DB, items []Event) error {
	ctx = context.WithValue(ctx, clickhouse.QueryID, fmt.Sprintf("%s-%d", "event-gen", rand.Uint64()))
	ctx, cancelFunc := context.WithTimeout(ctx, 10*time.Second)

	defer func() {
		cancelFunc()
	}()

	tx, err := cnn.BeginTx(ctx, nil)
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare(fmt.Sprintf("INSERT INTO %s.%s "+
		"("+
		"dateTime,"+
		"processingStartAt,"+
		"accountId,"+
		"messageHash,"+
		"messageHeader,"+
		"messageId,"+
		"applicationCode,"+
		"applicationGroupCode,"+
		"environment,"+
		"flowId,"+
		"flowType,"+
		"initialDeviceCount,"+
		"initialUserCount,"+
		"filteredCount,"+
		"processedCount,"+
		"sentCount,"+
		"confirmSentCount,"+
		"confirmDeliveredCount,"+
		"complainedCount,"+
		"bouncedCount"+
		")"+
		" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", "default", "processing"))
	if err != nil {
		_ = tx.Rollback()
		return err
	}

	for i := range items {
		if _, err = stmt.Exec(
			items[i].DateTime,
			items[i].ProcessingStartAt,
			items[i].AccountID,
			items[i].MessageHash,
			items[i].MessageHeader,
			items[i].MessageId,
			items[i].applicationCode,
			items[i].applicationGroupCode,
			items[i].environment,
			items[i].flowId,
			items[i].flowType,
			items[i].initialDeviceCount,
			items[i].initialUserCount,
			items[i].filteredCount,
			items[i].processedCount,
			items[i].sentCount,
			items[i].confirmSentCount,
			items[i].confirmDeliveredCount,
			items[i].complainedCount,
			items[i].bouncedCount,
		); err != nil {
			_ = tx.Rollback()
			return err
		}
	}

	if err = tx.Commit(); err != nil {
		_ = tx.Rollback()
		return err
	}

	return stmt.Close()
}

func fillAccounts() []Account {
	var accounts []Account
	for count := 0; count < AccountsCount; count++ {
		users := fillUsers()
		accounts = append(accounts, Account{Uuid: rand.Uint64(), Users: users, UsersCount: len(users)})
	}
	return accounts
}

func fillUsers() []User {
	var users []User
	for count := 0; count < rand.Intn(MaxAccountUsersCount)+1; count++ {
		users = append(users, User{Uuid: "user-" + uuid.New().String()})
	}
	return users
}

func randIndex(len int) int {
	switch len {
	case 0:
		return 0
	case 1:
		return 0
	default:
		return rand.Intn(len - 1)
	}
}

func ver(base, percent int) bool {
	return rand.Intn(base) < (base/100)*percent
}
