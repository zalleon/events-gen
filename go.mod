module gitlab.com/zalleon/events-gen

go 1.12

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/google/logger v1.0.1
	github.com/google/uuid v1.1.1
	github.com/kr/pretty v0.1.0 // indirect
	github.com/mailru/go-clickhouse v1.2.0
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.4.0 // indirect
	golang.org/x/sys v0.0.0-20190422165155-953cdadca894 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
